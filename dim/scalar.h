#pragma once
#include "pure/scalar.h"
#include <cmath>
#include <complex>
namespace dim
{

template<quantity Q = None>
using cscalar = gscalar<std::complex<double>, Q>;

template<typename T, quantity Q>
constexpr gscalar<std::complex<T>, Q> complexify(gscalar<T, Q> const s) noexcept {return std::complex<T>{s.value};}

template<typename T, quantity Q>
constexpr gscalar<T, Q> real(gscalar<std::complex<T>, Q> const s) noexcept {return std::real(s.value);}
template<typename T, quantity Q>
constexpr gscalar<T, Q> imag(gscalar<std::complex<T>, Q> const s) noexcept {return std::imag(s.value);}
template<typename T, quantity Q>
constexpr gscalar<T, pow<2>(Q)> norm(gscalar<std::complex<T>, Q> const s) noexcept {return std::norm(s.value);}
template<typename T, quantity Q>
constexpr gscalar<std::complex<T>, Q> conj(gscalar<std::complex<T>, Q> const s) noexcept {return std::conj(s.value);}

template<typename T, quantity Q>
gscalar<T, Q>  arg(gscalar<std::complex<T>, Q> const s) noexcept {return std:: arg(s.value);}
template<typename T, quantity Q>
gscalar<T, Q> proj(gscalar<std::complex<T>, Q> const s) noexcept {return std::proj(s.value);}
template<typename T, quantity Q>
gscalar<std::complex<T>, Q> polar(gscalar<T, Q> const r, gscalar<T, None> const phi) noexcept
{
    return std::polar(r.value, phi.value);
}

using std::min;
using std::max;

template<typename T, quantity Q>
gscalar<T, Q>             abs (gscalar<             T , Q> const s) noexcept {return std::abs(s.value);}
template<typename T, quantity Q>
gscalar<T, Q>             abs (gscalar<std::complex<T>, Q> const s) noexcept {return std::abs(s.value);}

template<std::int8_t M, std::uint8_t N = 1, typename T, quantity Q>
gscalar<T, pow<M, N>(Q)>  pow (gscalar<T, Q> const s) noexcept
{
    return std::pow(s.value, T(M) / T(N));
}
template<typename T, quantity Q>
gscalar<T, sqrt(Q)>       sqrt(gscalar<T, Q> const s) noexcept {return std::sqrt(s.value);}
template<typename T, quantity Q>
gscalar<T, pow<1, 3>(Q)>  cbrt(gscalar<T, Q> const s) noexcept {return std::cbrt(s.value);}

template<typename T>
gscalar<T, None> pow     (gscalar<T, None> const s, gscalar<T, None> const e) noexcept {return std::pow(s.value, e.value);}

template<typename T>
gscalar<T, None> exp     (gscalar<T, None> const s) noexcept {return std::exp    (s.value);}
template<typename T>
gscalar<T, None> expm1   (gscalar<T, None> const s) noexcept {return std::expm1  (s.value);}
template<typename T>
gscalar<T, None> log     (gscalar<T, None> const s) noexcept {return std::log    (s.value);}
template<typename T>
gscalar<T, None> log1p   (gscalar<T, None> const s) noexcept {return std::log1p  (s.value);}

template<typename T>
gscalar<T, None> erf     (gscalar<T, None> const s) noexcept {return std::erf    (s.value);}
template<typename T>
gscalar<T, None> erfc    (gscalar<T, None> const s) noexcept {return std::erfc   (s.value);}
template<typename T>
gscalar<T, None> lgamma  (gscalar<T, None> const s) noexcept {return std::lgamma (s.value);}
template<typename T>
gscalar<T, None> tgamma  (gscalar<T, None> const s) noexcept {return std::tgamma (s.value);}

template<typename T>
gscalar<T, None> sin     (gscalar<T, None> const s) noexcept {return std::sin    (s.value);}
template<typename T>
gscalar<T, None> cos     (gscalar<T, None> const s) noexcept {return std::cos    (s.value);}
template<typename T>
gscalar<T, None> tan     (gscalar<T, None> const s) noexcept {return std::tan    (s.value);}
template<typename T>
gscalar<T, None> asin    (gscalar<T, None> const s) noexcept {return std::asin   (s.value);}
template<typename T>
gscalar<T, None> acos    (gscalar<T, None> const s) noexcept {return std::acos   (s.value);}
template<typename T>
gscalar<T, None> atan    (gscalar<T, None> const s) noexcept {return std::atan   (s.value);}
template<typename T, quantity Q>
gscalar<T, None> atan2(gscalar<T, Q> const x, gscalar<T, Q> const y) noexcept {return std::atan2(x.value, y.value);}

template<typename T>
gscalar<T, None> sinh    (gscalar<T, None> const s) noexcept {return std::sinh   (s.value);}
template<typename T>
gscalar<T, None> cosh    (gscalar<T, None> const s) noexcept {return std::cosh   (s.value);}
template<typename T>
gscalar<T, None> tanh    (gscalar<T, None> const s) noexcept {return std::tanh   (s.value);}
template<typename T>
gscalar<T, None> asinh   (gscalar<T, None> const s) noexcept {return std::asinh  (s.value);}
template<typename T>
gscalar<T, None> acosh   (gscalar<T, None> const s) noexcept {return std::acosh  (s.value);}
template<typename T>
gscalar<T, None> atanh   (gscalar<T, None> const s) noexcept {return std::atanh  (s.value);}

template<typename T, quantity Q1, quantity Q2>
gscalar<T, mul(Q1, Q2)> fma( gscalar<T,     Q1     > const s1
                           , gscalar<T,         Q2 > const s2
                           , gscalar<T, mul(Q1, Q2)> const s3
                           ) noexcept
{
    return std::fma(s1.value, s2.value, s3.value);
}

} // namespace dim
