#pragma once
#include "scalar.h"
namespace dim
{

template<typename T>
struct vec3
{
    T x, y, z;

    constexpr vec3<T> &operator+=(vec3 const &v) noexcept {x += v.x; y += v.y; z += v.z; return *this;}
    constexpr vec3<T> &operator-=(vec3 const &v) noexcept {x -= v.x; y -= v.y; z -= v.z; return *this;}

    template<typename U>
    constexpr vec3<T> &operator*=(gscalar<U, dim::None> const s) noexcept {x *= s; y *= s; z *= s; return *this;}
    template<typename U>
    constexpr vec3<T> &operator/=(gscalar<U, dim::None> const s) noexcept {x /= s; y /= s; z /= s; return *this;}

    using iterator = T *;
    constexpr iterator          begin   ()       noexcept {return &x;}
    constexpr iterator          end     ()       noexcept {return &x + 3;}

    using const_iterator = T const *;
    constexpr const_iterator    begin   () const noexcept {return &x;}
    constexpr const_iterator    end     () const noexcept {return &x + 3;}
    constexpr const_iterator    cbegin  () const noexcept {return &x;}
    constexpr const_iterator    cend    () const noexcept {return &x + 3;}

    constexpr T       &operator[](unsigned int const i)       noexcept {assert(i < 3u); return begin()[i];}
    constexpr T const &operator[](unsigned int const i) const noexcept {assert(i < 3u); return begin()[i];}
};

template<typename T, quantity Q>
using gvector = vec3<gscalar<T, Q>>;
template<quantity Q = None>
using vector = vec3<scalar<Q>>;

template<typename T>
constexpr auto repeat(T const x) noexcept
    -> vec3<T>
{
    return {x, x, x};
}

template<typename T>
constexpr auto dropDimension(vec3<T> const &v) noexcept
    -> vec3<decltype(dropDimension(T()))>
{
    return
    {
        dropDimension(v.x),
        dropDimension(v.y),
        dropDimension(v.z),
    };
}
template<quantity Q, typename T>
constexpr auto pickDimension(vec3<T> const &v) noexcept
    -> vec3<decltype(pickDimension<Q>(T()))>
{
    return
    {
        pickDimension<Q>(v.x),
        pickDimension<Q>(v.y),
        pickDimension<Q>(v.z),
    };
}

template<typename T>
constexpr vec3<T> operator+(vec3<T> const &v1, vec3<T> const &v2) noexcept
{
    return {v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};
}
template<typename T>
constexpr vec3<T> operator-(vec3<T> const &v1, vec3<T> const &v2) noexcept
{
    return {v1.x - v2.x, v1.y - v2.y, v1.z - v2.z};
}
template<typename T>
constexpr vec3<T> operator-(vec3<T> const &v) noexcept
{
    return {-v.x, -v.y, -v.z};
}
template<typename T, typename U>
constexpr vec3<T> operator*(vec3<T> const &v, U const s) noexcept
{
    return {v.x * s, v.y * s, v.z * s};
}
template<typename T, typename U>
constexpr vec3<T> operator*(U const s, vec3<T> const &v) noexcept
{
    return {s * v.x, s * v.y, s * v.z};
}
template<typename T, typename U>
constexpr vec3<T> operator/(vec3<T> const &v, U const s) noexcept
{
    return {v.x / s, v.y / s, v.z / s};
}

template<typename T, quantity Q1, quantity Q2>
constexpr gvector<T, mul(Q1, Q2)> operator*(gvector<T, Q1> const &v, gscalar<T, Q2> const s) noexcept
{
    return {v.x * s, v.y * s, v.z * s};
}
template<typename T, quantity Q1, quantity Q2>
constexpr gvector<T, mul(Q1, Q2)> operator*(gscalar<T, Q1> const s, gvector<T, Q2> const &v) noexcept
{
    return {s * v.x, s * v.y, s * v.z};
}
template<typename T, quantity Q1, quantity Q2>
constexpr gvector<T, div(Q1, Q2)> operator/(gvector<T, Q1> const &v, gscalar<T, Q2> const s) noexcept
{
    return {v.x / s, v.y / s, v.z / s};
}

template<typename T, quantity Q1, quantity Q2>
constexpr gvector<T, mul(Q1, Q2)> operator*(gvector<T, Q1> const &v1, gvector<T, Q2> const &v2) noexcept
{
    return {v1.x * v2.x, v1.y * v2.y, v1.z * v2.z};
}
template<typename T, quantity Q1, quantity Q2>
constexpr gvector<T, div(Q1, Q2)> operator/(gvector<T, Q1> const &v1, gvector<T, Q2> const &v2) noexcept
{
    return {v1.x / v2.x, v1.y / v2.y, v1.z / v2.z};
}

template<typename T, quantity Q1, quantity Q2>
constexpr gscalar<T, mul(Q1, Q2)> dot(gvector<T, Q1> const &v1, gvector<T, Q2> const &v2) noexcept
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}
template<typename T, quantity Q>
constexpr gscalar<T, mul(Q, Q)> square(gvector<T, Q> const &v) noexcept
{
    return dot(v, v);
}
template<typename T, quantity Q1, quantity Q2>
constexpr gvector<T, mul(Q1, Q2)> cross(gvector<T, Q1> const &v1, gvector<T, Q2> const &v2) noexcept
{
    return
    {
        v1.y * v2.z - v1.z * v2.y,
        v1.z * v2.x - v1.x * v2.z,
        v1.x * v2.y - v1.y * v2.x,
    };
}

} // namespace dim
