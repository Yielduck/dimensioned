#pragma once
#include "../quantity.h"
namespace dim
{

template<typename T, quantity Q>
struct gscalar
{
    T value;

    gscalar() = default;
    constexpr gscalar(T const s) noexcept
        : value(s)
    {}
    template<typename U>
    constexpr gscalar(gscalar<U, Q> const s) noexcept
        : value(s.value)
    {}

    constexpr gscalar<T, Q> &operator+=(gscalar const s) noexcept {value += s.value; return *this;}
    constexpr gscalar<T, Q> &operator-=(gscalar const s) noexcept {value -= s.value; return *this;}
    constexpr gscalar<T, Q> &operator+=(T const s) noexcept {value += s; return *this;}
    constexpr gscalar<T, Q> &operator-=(T const s) noexcept {value -= s; return *this;}
    constexpr gscalar<T, Q> &operator*=(gscalar<T, None> const s) noexcept {value *= s.value; return *this;}
    constexpr gscalar<T, Q> &operator/=(gscalar<T, None> const s) noexcept {value /= s.value; return *this;}
};

template<quantity Q = None>
using scalar = gscalar<double, Q>;

template<typename U, typename T, quantity Q>
constexpr U cast(gscalar<T, Q> const s) noexcept {return static_cast<U>(s.value);}

template<quantity Q, typename T>
constexpr gscalar<T, None> dropDimension(gscalar<T,    Q> const s) noexcept {return s.value;}
template<quantity Q, typename T>
constexpr gscalar<T,    Q> pickDimension(gscalar<T, None> const s) noexcept {return s.value;}

template<typename T, quantity Q>
constexpr gscalar<T, Q> operator+(gscalar<T, Q> const s1, gscalar<T, Q> const s2) noexcept {return s1.value + s2.value;}
template<typename T, quantity Q>
constexpr gscalar<T, Q> operator+(gscalar<T, Q> const s1, T             const s2) noexcept {return s1.value + s2;}
template<typename T, quantity Q>
constexpr gscalar<T, Q> operator+(T             const s1, gscalar<T, Q> const s2) noexcept {return s1 + s2.value;}

template<typename T, quantity Q>
constexpr gscalar<T, Q> operator-(gscalar<T, Q> const s1, gscalar<T, Q> const s2) noexcept {return s1.value - s2.value;}
template<typename T, quantity Q>
constexpr gscalar<T, Q> operator-(gscalar<T, Q> const s1, T             const s2) noexcept {return s1.value - s2;}
template<typename T, quantity Q>
constexpr gscalar<T, Q> operator-(T             const s1, gscalar<T, Q> const s2) noexcept {return s1 - s2.value;}

template<typename T, quantity Q>
constexpr gscalar<T, Q> operator-(gscalar<T, Q> const s) noexcept {return -s.value;}

template<typename T, typename U, quantity Q>
constexpr gscalar<T, Q> operator*(gscalar<T, Q> const s1, U const s2) noexcept {return s1.value * s2;}
template<typename T, typename U, quantity Q>
constexpr gscalar<T, Q> operator*(U const s1, gscalar<T, Q> const s2) noexcept {return s1 * s2.value;}

template<typename T, typename U, quantity Q>
constexpr gscalar<T, Q> operator/(gscalar<T, Q> const s1, U const s2) noexcept {return s1.value / s2;}
template<typename T, typename U, quantity Q>
constexpr gscalar<T, inv(Q)> operator/(U const s1, gscalar<T, Q> const s2) noexcept {return s1 / s2.value;}

template<typename T, quantity Q1, quantity Q2>
constexpr gscalar<T, mul(Q1, Q2)> operator*(gscalar<T, Q1> const s1, gscalar<T, Q2> const s2) noexcept {return s1.value * s2.value;}
template<typename T, quantity Q1, quantity Q2>
constexpr gscalar<T, div(Q1, Q2)> operator/(gscalar<T, Q1> const s1, gscalar<T, Q2> const s2) noexcept {return s1.value / s2.value;}

template<typename T, quantity Q>
constexpr gscalar<T, Q> min(gscalar<T, Q> const s1, gscalar<T, Q> const s2) noexcept {return s1.value < s2.value ? s1 : s2;}
template<typename T, quantity Q>
constexpr gscalar<T, Q> max(gscalar<T, Q> const s1, gscalar<T, Q> const s2) noexcept {return s1.value < s2.value ? s2 : s1;}

template<typename T, quantity Q>
constexpr bool operator< (gscalar<T, Q> const s1, gscalar<T, Q> const s2) noexcept {return s1.value < s2.value;}
template<typename T, quantity Q>
constexpr bool operator> (gscalar<T, Q> const s1, gscalar<T, Q> const s2) noexcept {return s1.value > s2.value;}
template<typename T, quantity Q>
constexpr bool operator<=(gscalar<T, Q> const s1, gscalar<T, Q> const s2) noexcept {return s1.value <= s2.value;}
template<typename T, quantity Q>
constexpr bool operator>=(gscalar<T, Q> const s1, gscalar<T, Q> const s2) noexcept {return s1.value >= s2.value;}
template<typename T, quantity Q>
constexpr bool operator==(gscalar<T, Q> const s1, gscalar<T, Q> const s2) noexcept {return s1.value == s2.value;}
template<typename T, quantity Q>
constexpr bool operator!=(gscalar<T, Q> const s1, gscalar<T, Q> const s2) noexcept {return s1.value != s2.value;}

template<typename T, quantity Q>
constexpr bool operator< (gscalar<T, Q> const s1, T             const s2) noexcept {return s1.value < s2;}
template<typename T, quantity Q>
constexpr bool operator> (gscalar<T, Q> const s1, T             const s2) noexcept {return s1.value > s2;}
template<typename T, quantity Q>
constexpr bool operator<=(gscalar<T, Q> const s1, T             const s2) noexcept {return s1.value <= s2;}
template<typename T, quantity Q>
constexpr bool operator>=(gscalar<T, Q> const s1, T             const s2) noexcept {return s1.value >= s2;}
template<typename T, quantity Q>
constexpr bool operator==(gscalar<T, Q> const s1, T             const s2) noexcept {return s1.value == s2;}
template<typename T, quantity Q>
constexpr bool operator!=(gscalar<T, Q> const s1, T             const s2) noexcept {return s1.value != s2;}

template<typename T, quantity Q>
constexpr bool operator< (T             const s1, gscalar<T, Q> const s2) noexcept {return s1 < s2.value;}
template<typename T, quantity Q>
constexpr bool operator> (T             const s1, gscalar<T, Q> const s2) noexcept {return s1 > s2.value;}
template<typename T, quantity Q>
constexpr bool operator<=(T             const s1, gscalar<T, Q> const s2) noexcept {return s1 <= s2.value;}
template<typename T, quantity Q>
constexpr bool operator>=(T             const s1, gscalar<T, Q> const s2) noexcept {return s1 >= s2.value;}
template<typename T, quantity Q>
constexpr bool operator==(T             const s1, gscalar<T, Q> const s2) noexcept {return s1 == s2.value;}
template<typename T, quantity Q>
constexpr bool operator!=(T             const s1, gscalar<T, Q> const s2) noexcept {return s1 != s2.value;}

} // namespace dim
