#pragma once
#include "vector.h"
namespace dim
{

template<typename T, quantity Q>
using gmatrix = vec3<gvector<T, Q>>;
template<quantity Q = None>
using matrix = vec3<vector<Q>>;

template<typename T = double>
static constexpr gmatrix<T, None> identity =
{
    {T(1), T(0), T(0)},
    {T(0), T(1), T(0)},
    {T(0), T(0), T(1)},
};

template<typename T, quantity Q>
constexpr gmatrix<T, Q> fromDiagonal(gvector<T, Q> const &v) noexcept
{
    return
    {
        {v[0], T(0), T(0)},
        {T(0), v[1], T(0)},
        {T(0), T(0), v[2]},
    };
}

template<typename T, quantity Q>
constexpr gmatrix<T, Q> skew(gvector<T, Q> const &v) noexcept
{
    return
    {
        {T(0), -v.z,  v.y},
        { v.z, T(0), -v.x},
        {-v.y,  v.x, T(0)},
    };
}

template<typename T, quantity Q>
constexpr gvector<T, Q> diagonal(gmatrix<T, Q> const &m) noexcept
{
    return {m[0][0], m[1][1], m[2][2]};
}

template<typename T, quantity Q>
constexpr gscalar<T, Q> trace(gmatrix<T, Q> const &m) noexcept
{
    return m[0][0] + m[1][1] + m[2][2];
}
template<typename T, quantity Q>
constexpr gscalar<T, pow<2>(Q)> I2(gmatrix<T, Q> const &m) noexcept
{
    return m[0][0] * m[1][1] - m[0][1] * m[1][0]
         + m[1][1] * m[2][2] - m[1][2] * m[2][1]
         + m[2][2] * m[0][0] - m[2][0] * m[0][2];
}
template<typename T, quantity Q>
constexpr gscalar<T, pow<3>(Q)> determinant(gmatrix<T, Q> const &m) noexcept
{
    return m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1])
         + m[0][1] * (m[1][2] * m[2][0] - m[1][0] * m[2][2])
         + m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);
}

template<typename T, quantity Q>
constexpr gmatrix<T, Q> transpose(gmatrix<T, Q> const &m) noexcept
{
    return
    {
        {m[0][0], m[1][0], m[2][0]},
        {m[0][1], m[1][1], m[2][1]},
        {m[0][2], m[1][2], m[2][2]},
    };
}
template<typename T, quantity Q>
constexpr gmatrix<T, inv(Q)> inverse(gmatrix<T, Q> const &m) noexcept
{
    auto const det2 = [&m](unsigned int const i, unsigned int const j) noexcept
        -> gscalar<T, pow<2>(Q)>
    {
        unsigned int const i1 = (i + 1u) % 3u;
        unsigned int const j1 = (j + 1u) % 3u;
        unsigned int const i2 = (i + 2u) % 3u;
        unsigned int const j2 = (j + 2u) % 3u;
        return m[i1][j1] * m[i2][j2] - m[i1][j2] * m[i2][j1];
    };
    auto const det3 = determinant(m);
    assert(det3 != T(0));
    return gmatrix<T, pow<2>(Q)>
    {
        {det2(0, 0), det2(1, 0), det2(2, 0)},
        {det2(0, 1), det2(1, 1), det2(2, 1)},
        {det2(0, 2), det2(1, 2), det2(2, 2)},
    } / det3;
}

template<typename T, quantity Q1, quantity Q2>
constexpr gmatrix<T, mul(Q1, Q2)> outer(gvector<T, Q1> const &v1, gvector<T, Q2> const &v2) noexcept
{
    return
    {
        {v1[0] * v2[0], v1[0] * v2[1], v1[0] * v2[2]},
        {v1[1] * v2[0], v1[1] * v2[1], v1[1] * v2[2]},
        {v1[2] * v2[0], v1[2] * v2[1], v1[2] * v2[2]},
    };
}
template<typename T, quantity Q1, quantity Q2>
constexpr gmatrix<T, mul(Q1, Q2)> operator*(gmatrix<T, Q1> const &m1, gmatrix<T, Q2> const &m2) noexcept
{
    auto const mdot = [&](unsigned int const i, unsigned int const j) noexcept
    {
        return m1[i][0] * m2[0][j] + m1[i][1] * m2[1][j] + m1[i][2] * m2[2][j];
    };
    return
    {
        {mdot(0, 0), mdot(0, 1), mdot(0, 2)},
        {mdot(1, 0), mdot(1, 1), mdot(1, 2)},
        {mdot(2, 0), mdot(2, 1), mdot(2, 2)},
    };
}
template<typename T, quantity Q1, quantity Q2>
constexpr gvector<T, mul(Q1, Q2)> operator*(gmatrix<T, Q1> const &m, gvector<T, Q2> const v) noexcept
{
    return
    {
        dot(m[0], v),
        dot(m[1], v),
        dot(m[2], v),
    };
}
template<typename T, quantity Q1, quantity Q2>
constexpr gmatrix<T, mul(Q1, Q2)> operator*(gmatrix<T, Q1> const &m, gscalar<T, Q2> const s) noexcept
{
    return {m[0] * s, m[1] * s, m[2] * s};
}
template<typename T, quantity Q1, quantity Q2>
constexpr gmatrix<T, mul(Q1, Q2)> operator*(gscalar<T, Q1> const s, gmatrix<T, Q2> const &m) noexcept
{
    return m * s;
}
template<typename T, quantity Q1, quantity Q2>
constexpr gmatrix<T, div(Q1, Q2)> operator/(gmatrix<T, Q1> const &m, gscalar<T, Q2> const s) noexcept
{
    return {m[0] / s, m[1] / s, m[2] / s};
}
template<typename T, quantity Q1, quantity Q2>
constexpr gscalar<T, mul(Q1, Q2)> dot(gmatrix<T, Q1> const &m1, gmatrix<T, Q2> const &m2) noexcept
{
    return m1[0][0] * m2[0][0] + m1[0][1] * m2[0][1] + m1[0][2] * m2[0][2]
         + m1[1][0] * m2[1][0] + m1[1][1] * m2[1][1] + m1[1][2] * m2[1][2]
         + m1[2][0] * m2[2][0] + m1[2][1] * m2[2][1] + m1[2][2] * m2[2][2];
}

} // namespace dim
