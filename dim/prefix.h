#include "pure/scalar.h"
namespace dim
{

namespace prefix
{

static constexpr scalar<None> yocto = 1e-24;
static constexpr scalar<None> zepto = 1e-21;
static constexpr scalar<None>  atto = 1e-18;
static constexpr scalar<None> femto = 1e-15;
static constexpr scalar<None>  pico = 1e-12;
static constexpr scalar<None>  nano = 1e-9;
static constexpr scalar<None> micro = 1e-6;
static constexpr scalar<None> milli = 1e-3;
static constexpr scalar<None> centi = 1e-2;
static constexpr scalar<None>  deci = 1e-1;

static constexpr scalar<None>  deca = 1e1;
static constexpr scalar<None> hecto = 1e2;
static constexpr scalar<None>  kilo = 1e3;
static constexpr scalar<None>  mega = 1e6;
static constexpr scalar<None>  giga = 1e9;
static constexpr scalar<None>  tera = 1e12;
static constexpr scalar<None>  peta = 1e15;
static constexpr scalar<None>   exa = 1e18;
static constexpr scalar<None> zetta = 1e21;
static constexpr scalar<None> yotta = 1e24;

} // namespace prefix

} // namespace dim
