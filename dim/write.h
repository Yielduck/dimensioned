#pragma once
#include "vector.h"
#include <ostream>
namespace dim
{

inline std::ostream &write(std::ostream &out, quantity const Q)
{
    char const *base[] =
    {
        "s",
        "m",
        "kg",
        "A",
        "K",
        "mol",
        "cd",
        "bit",
    };
    if(Q != None)
    {
        using i64 = std::int64_t;
        auto const superscript = [](i64 const i) noexcept
            -> char const *
        {
            switch(i)
            {
                case -7: return "⁻⁷";
                case -6: return "⁻⁶";
                case -5: return "⁻⁵";
                case -4: return "⁻⁴";
                case -3: return "⁻³";
                case -2: return "⁻²";
                case -1: return "⁻¹";
                case  1: return "";
                case  2: return "²";
                case  3: return "³";
                case  4: return "⁴";
                case  5: return "⁵";
                case  6: return "⁶";
                case  7: return "⁷";
                case  8: return "⁸";
                default: assert(0); return "";
            }
        };

        unsigned int const count = sizeof(base) / sizeof(*base);
        struct Entry
        {
            i64 i;
            i64 power;
        } stack[count];
        i64 size = 0;

        for(i64 i = 0; i < count; ++i)
        {
            i64 const power = extract(Q, i);
            if(power != 0)
                stack[size++] = {i, power};
        }

        while(true)
        {
            Entry const e = stack[--size];
            out << base[e.i] << superscript(e.power);
            if(size == 0)
                break;
            out << "·";
        }
    }
    return out;
}

template<typename T, dim::quantity Q>
std::ostream &operator<<(std::ostream &out, dim::gscalar<T, Q> const s)
{
    out << s.value;
    write(out, Q);
    return out;
}
template<typename T, dim::quantity Q>
std::ostream &operator<<(std::ostream &out, dim::gvector<T, Q> const v)
{
    out << "(" << v.x.value << ", " << v.y.value << ", " << v.z.value << ")";
    write(out, Q);
    return out;
}
template<typename T, dim::quantity Q>
std::ostream &operator<<(std::ostream &out, dim::gmatrix<T, Q> const m)
{
    out << "(";
    out << "(" << m.x.x.value << ", " << m.x.y.value << ", " << m.x.z.value << "), ";
    out << "(" << m.y.x.value << ", " << m.y.y.value << ", " << m.y.z.value << "), ";
    out << "(" << m.z.x.value << ", " << m.z.y.value << ", " << m.z.z.value << "))";
    write(out, Q);
    return out;
}

} // namespace dim
