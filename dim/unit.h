#pragma once
#include "constant.h"
#include "prefix.h"
namespace dim
{

namespace unit
{

static constexpr scalar<             Length> meter       = 1;
static constexpr scalar<               Time> second      = 1;
static constexpr scalar<               Mass> kilogram    = 1;
static constexpr scalar<    ElectricCurrent> ampere      = 1;
static constexpr scalar<        Temperature> kelvin      = 1;
static constexpr scalar<  AmountOfSubstance> mol         = 1;
static constexpr scalar<  LuminousIntensity> candela     = 1;
static constexpr scalar<        Information> bit         = 1;


static constexpr scalar<               Area> squareMeter = 1;
static constexpr scalar<             Volume> cubicMeter  = 1;

static constexpr scalar<          Frequency> hertz       = 1;
static constexpr scalar<              Force> newton      = 1;
static constexpr scalar<             Energy> joule       = 1;
static constexpr scalar<              Power> watt        = 1;
static constexpr scalar<           Pressure> pascal      = 1;

static constexpr scalar<     ElectricCharge> coulomb     = 1;
static constexpr scalar<            Voltage> volt        = 1;
static constexpr scalar<         Resistance> ohm         = 1;
static constexpr scalar<   ElectricCapacity> farad       = 1;
static constexpr scalar<  MagneticInduction> tesla       = 1;


static constexpr scalar<Time> minute = 60 * second;
static constexpr scalar<Time> hour   = 60 * minute;
static constexpr scalar<Time> day    = 24 * hour;
static constexpr scalar<Time> year   = 365.25 * day;


static constexpr scalar<Length> angstrom = 1e-10 * meter;

static constexpr scalar<Length> AU = 149597870700. * meter;

static constexpr scalar<Length> inch = 0.0254 * meter;
static constexpr scalar<Length> foot = 0.3048 * meter;
static constexpr scalar<Length> yard = 0.9144 * meter;
static constexpr scalar<Length> mile = 1760 * yard;
static constexpr scalar<Length> nauticalMile = 1852 * meter;


static constexpr scalar<Volume> liter = prefix::milli * cubicMeter;

static constexpr scalar<Volume> cubicInch = inch * inch * inch;
static constexpr scalar<Volume> gallon = 231 * cubicInch;
static constexpr scalar<Volume> barrel = 42 * gallon;


static constexpr scalar<Mass> gram = 1e-3 * kilogram;
static constexpr scalar<Mass> tonne = 1e3 * kilogram;

static constexpr scalar<Mass> ounce = 0.028349523125 * kilogram;
static constexpr scalar<Mass> pound = 16 * ounce;

static constexpr scalar<Mass> dalton = 1.66053906660e-27 * kilogram;


static constexpr scalar<None> radian = 1;
static constexpr scalar<None> pi = 3.141592653589793;
static constexpr scalar<None> degree = pi / 180;
static constexpr scalar<None> arcminute = degree / 60;
static constexpr scalar<None> arcsecond = arcminute / 60;


static constexpr scalar<Acceleration> g = 9.80665 * meter / second / second;


static constexpr scalar<Force> kilogramForce = g * kilogram;
static constexpr scalar<Force>    poundForce = g * pound;


static constexpr scalar<Energy> calorie = 4.184 * joule;
static constexpr scalar<Energy> ev = constant::e * volt;
static constexpr scalar<Energy> footPound = foot * poundForce;


static constexpr scalar<Power> horsepower = 33000 * footPound / minute;


static constexpr scalar<Pressure> bar = 100000 * pascal;
static constexpr scalar<Pressure> atmosphere = 101325 * pascal;
static constexpr scalar<Pressure> torr = atmosphere / 760;


static constexpr scalar<        Information> byte = 8;

} // namespace unit

} // namespace dim
