#pragma once
#include "cubic.h"
#include "pure/matrix.h"

namespace dim
{

template<quantity Q>
using cmatrix = vec3<cvector<Q>>;

template<typename T, quantity Q>
constexpr CubicPolynomial<T, Q> characteristicPolynomial(gmatrix<T, Q> const &m) noexcept
{
    return {-trace(m), I2(m), -determinant(m)};
}

template<typename T, quantity Q>
auto eigenvalues(gmatrix<T, Q> const &m) noexcept
{
    return solveCubicEquation(characteristicPolynomial(m));
}

template<typename T, quantity Q>
gmatrix<std::complex<T>, None> vandermonde(ClassifiedSolution<T, Q> const &eigen) noexcept
{
    using C = std::complex<T>;
    auto const e = dropDimension(eigen.value);
    switch(eigen.kind)
    {
        case SolutionKind::Distinct:
            return
            {
                {       C(1),        C(1),        C(1)},
                {       e[0],        e[1],        e[2]},
                {e[0] * e[0], e[1] * e[1], e[2] * e[2]},
            };
        case SolutionKind::TwiceDegenerate:
            return
            {
                {       C(1),        C(1),        C(0)},
                {       e[0],        e[1],        C(1)},
                {e[0] * e[0], e[1] * e[1], e[1] * C(2)},
            };
        case SolutionKind::ThriceDegenerate:
            return
            {
                {       C(1),        C(0),        C(0)},
                {       e[0],        C(1),        C(0)},
                {e[0] * e[0], e[0] * C(2),        C(2)},
            };
        default: assert(0); return {};
    }
}

template<typename T, quantity Q>
struct phi3
    /* phi3 : (R, +) -> GL(3, C) is a continuous group homomorphism;
     * one may prove that this map is differentiable;
     * let A be the derivative of phi3 in zero: A = d/dt(phi3(t))[t = 0]
     * then phi3(t) = exp(A * t)
     */
{
    using C = std::complex<T>;

    gmatrix<C, Q> operator()(gscalar<C, inv(Q)> const t) const noexcept
    {
        return pickDimension<Q>(evaluate(dropDimension(t)));
    }
    gmatrix<T, Q> operator()(gscalar<T, inv(Q)> const t) const noexcept
    {
        return real(this->operator()(complexify(t)));
    }

private:
    /* using Cayley-Hamilton theorem one may prove the following:
     * given e0, e1, e2 are eigenvalues of A, there always are matrices M0, M1, M2, satisfying
     * exp(A * t) = M0 * exp(e0 * t) + M1 * exp(e1 * t)     + M2 * exp(e2 * t)           | e0 != e1 != e2
     * exp(A * t) = M0 * exp(e0 * t) + M1 * exp(e1 * t)     + M2 * exp(e1 * t) * t       | e0 != e1 == e2
     * exp(A * t) = M0 * exp(e0 * t) + M1 * exp(e0 * t) * t + M2 * exp(e0 * t) * t * t   | e0 == e1 == e2
     * so it's possible to define an instance of phi3 in terms of M_i and e_i:
     */
    ClassifiedSolution<T, None> eigen;
    gmatrix<C, None> M[3];

    gmatrix<C, None> evaluate(gscalar<C, None> const t) const noexcept
    {
        gvector<C, None> const &e = eigen.value;
        switch(eigen.kind)
        {
            case SolutionKind::Distinct:
                return M[0] *  exp(e[0] * t)
                     + M[1] *  exp(e[1] * t)
                     + M[2] *  exp(e[2] * t);
            case SolutionKind::TwiceDegenerate:
                return M[0] *  exp(e[0] * t)
                     + M[1] *  exp(e[1] * t)
                     + M[2] * (exp(e[2] * t) * t);
            case SolutionKind::ThriceDegenerate:
                return M[0] *  exp(e[0] * t)
                     + M[1] * (exp(e[1] * t) * t    )
                     + M[2] * (exp(e[2] * t) * t * t);
            default: assert(0); return {};
        }
    }

public:
    /* M_i are the solutions to the following linear equation system:
     *          phi3 (0) = I
     *     d/dt(phi3)(0) = A
     *   d2/dt2(phi3)(0) = A * A
     *
     * for example, when e0 != e1, e1 != e2, e2 != e0,
     *                  exp(A * t) = M0 * exp(e0 * t)           + M1 * exp(e1 * t)           + M2 * exp(e2 * t)
     * d/dt:        A * exp(A * t) = M0 * exp(e0 * t) * e0      + M1 * exp(e1 * t) * e1      + M2 * exp(e2 * t) * e2
     * d2/dt2:  A * A * exp(A * t) = M0 * exp(e0 * t) * e0 * e0 + M1 * exp(e1 * t) * e1 * e1 + M2 * exp(e2 * t) * e2 * e2
     * t = 0:
     * /       1        1        1 \ / M0 \   /  I  \
     * |      e0       e1       e2 | | M1 | = |  A  |
     * \ e0 * e0  e1 * e1  e2 * e2 / \ M2 /   \A * A/
     *
     * in general, (M0, M1, M2) = inverse(vandermonde(e0, e1, e2)) * (I, A, A * A)
     */
    phi3(gmatrix<C, Q> const &m) noexcept
        : eigen(eigenvalues(dropDimension(m)))
    {
        gmatrix<C, None> const A = dropDimension(m);

        gmatrix<C, None> const W = inverse(vandermonde(eigen));
        gmatrix<C, None> const I = identity<C>;
        gmatrix<C, None> const A2 = A * A;

        M[0] = I * W[0][0] + A * W[0][1] + A2 * W[0][2];
        M[1] = I * W[1][0] + A * W[1][1] + A2 * W[1][2];
        M[2] = I * W[2][0] + A * W[2][1] + A2 * W[2][2];
    }
    phi3(gmatrix<T, Q> const &A) noexcept
        : phi3(complexify(A))
    {}
};

} // namespace dim
