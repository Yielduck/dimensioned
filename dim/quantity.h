#pragma once
#include <cstdint>
#include <cassert>
namespace dim
{

// 4 bits for each dimension (dimension values may vary from -7 to 8)
using quantity = std::int64_t;

static constexpr quantity None                  = 0x777777777777777;

static constexpr quantity Time                  = 0x777777777777778;
static constexpr quantity Length                = 0x777777777777787;
static constexpr quantity Mass                  = 0x777777777777877;
static constexpr quantity ElectricCurrent       = 0x777777777778777;
static constexpr quantity Temperature           = 0x777777777787777;
static constexpr quantity AmountOfSubstance     = 0x777777777877777;
static constexpr quantity LuminousIntensity     = 0x777777778777777;

static constexpr quantity Information           = 0x777777787777777;

inline constexpr quantity extract(quantity const Q, std::int64_t const i) noexcept
{
    return ((Q >> (i * 4)) & 0xf) - 0x7;
}
inline constexpr quantity time             (quantity const Q) noexcept {return extract(Q, 0);}
inline constexpr quantity length           (quantity const Q) noexcept {return extract(Q, 1);}
inline constexpr quantity mass             (quantity const Q) noexcept {return extract(Q, 2);}
inline constexpr quantity electricCurrent  (quantity const Q) noexcept {return extract(Q, 3);}
inline constexpr quantity temperature      (quantity const Q) noexcept {return extract(Q, 4);}
inline constexpr quantity amountOfSubstance(quantity const Q) noexcept {return extract(Q, 5);}
inline constexpr quantity luminousIntensity(quantity const Q) noexcept {return extract(Q, 6);}
inline constexpr quantity information      (quantity const Q) noexcept {return extract(Q, 7);}

inline constexpr quantity mul(quantity const lhs, quantity const rhs) noexcept
{
    return lhs + rhs - None;
}
inline constexpr quantity div(quantity const lhs, quantity const rhs) noexcept
{
    return lhs - rhs + None;
}
template<std::int8_t M, std::uint8_t N = 1>
constexpr quantity pow(quantity const Q) noexcept
 // -> pow(Q, M / N)
{
    std::int64_t const m = M;
    std::int64_t const n = N;
    auto const nom = Q * m + None * (n - m);
    assert(nom % n == 0);
    return nom / n;
}
inline constexpr quantity inv(quantity const Q) noexcept
{
    return pow<-1>(Q);
}
inline constexpr quantity sqrt(quantity const Q) noexcept
{
    return pow<1, 2>(Q);
}

} // namespace dim
