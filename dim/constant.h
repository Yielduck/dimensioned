#pragma once
#include "pure/scalar.h"
#include "derived.h"
namespace dim
{

namespace constant
{

// SI defining constants
static constexpr scalar<             Frequency> nu_Cs = 9.192631770e9;
static constexpr scalar<              Velocity>     c = 2.99792458e8;
static constexpr scalar<                Action>     h = 6.62607015e-34;
static constexpr scalar<        ElectricCharge>     e = 1.602176634e-19;
static constexpr scalar<          HeatCapacity>   k_B = 1.380649e-23;
static constexpr scalar<inv(AmountOfSubstance)>   N_a = 6.02214076e23;
static constexpr scalar<             Frequency>  K_cd = 6.83e2;

// other constants
static constexpr scalar<                  None>     a = 0.0072973525693; // fine structure constant
static constexpr scalar<           inv(Length)>    Ry = 10973731.568160;

static constexpr auto  mu0 = 2 * a * h / (e * e * c);
static constexpr auto eps0 = e * e / (2 * a * h * c);

static constexpr auto m_e = 2 * Ry * h / (c * a * a);
static constexpr auto m_p = 1836.152673437 * m_e;

} // namespace constant

} // namespace dim
