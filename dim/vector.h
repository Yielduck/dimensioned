#pragma once
#include "scalar.h"
#include "pure/vector.h"
namespace dim
{

template<typename T>
gvector<T, None> direction(gscalar<T, None> const theta, gscalar<T, None> const phi) noexcept
{
    return
    {
        dim::cos(theta) * dim::cos(phi),
        dim::cos(theta) * dim::sin(phi),
        dim::sin(theta),
    };
}

template<quantity Q = None>
using cvector = vec3<cscalar<Q>>;

template<typename T, quantity Q>
gscalar<T, Q> length(gvector<T, Q> const &v) noexcept
{
    return sqrt(square(v));
}
template<typename T, quantity Q>
gvector<T, None> normalize(gvector<T, Q> const &v) noexcept
{
    return v / length(v);
}

template<typename T>
auto abs(vec3<T> const &v) noexcept
    -> vec3<decltype(abs(T()))>
{
    return
    {
        abs(v.x),
        abs(v.y),
        abs(v.z),
    };
}

template<typename T>
constexpr auto min(vec3<T> const &v1, vec3<T> const &v2) noexcept
    -> vec3<decltype(min(T(), T()))>
{
    return
    {
        min(v1.x, v2.x),
        min(v1.y, v2.y),
        min(v1.z, v2.z),
    };
}
template<typename T, quantity Q>
constexpr auto max(vec3<T> const &v1, vec3<T> const &v2) noexcept
    -> vec3<decltype(max(T(), T()))>
{
    return
    {
        max(v1.x, v2.x),
        max(v1.y, v2.y),
        max(v1.z, v2.z),
    };
}

template<typename T>
constexpr auto complexify(vec3<T> const &v) noexcept
    -> vec3<decltype(complexify(T()))>
{
    return
    {
        complexify(v.x),
        complexify(v.y),
        complexify(v.z),
    };
}
template<typename T>
constexpr auto real(vec3<T> const &v) noexcept
    -> vec3<decltype(real(T()))>
{
    return
    {
        real(v.x),
        real(v.y),
        real(v.z),
    };
}
template<typename T>
constexpr auto imag(vec3<T> const &v) noexcept
    -> vec3<decltype(imag(T()))>
{
    return
    {
        imag(v.x),
        imag(v.y),
        imag(v.z),
    };
}

} // namespace dim
