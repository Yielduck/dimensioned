#pragma once
#include "vector.h"
#include <limits>
namespace dim
{

template<typename T, quantity Q>
struct CubicPolynomial
{
    using S = gscalar<T, Q>;
    // x^3 + a * x^2 + b * x + c = 0
    gscalar<T,        Q > a;
    gscalar<T, pow<2>(Q)> b;
    gscalar<T, pow<3>(Q)> c;

    constexpr auto operator()(S const x) const noexcept
    {
        return c + x * (b + x * (a + x));
    }
    constexpr auto dfdx(S const x) const noexcept
    {
        return b + x * (T(2) * a + T(3) * x);
    }
    constexpr auto df2dx2(S const x) const noexcept
    {
        return T(6) * x + T(2) * a;
    }
};

template<typename T, quantity Q>
auto solveCubicEquation(CubicPolynomial<std::complex<T>, Q> const &poly) noexcept
{
    using C = std::complex<T>;
    using S = gscalar<C, None>;
    auto const s = +[](T const x) noexcept {return S({x, 0});};

    auto const p = poly.a * (s(4.5) * poly.b - poly.a * poly.a) - s(13.5) * poly.c;
    auto const q = poly.a * poly.a - s(3) * poly.b;
    // resolvent: u^2 - 2*p*u + q^3 = 0  ->  u1, u2
    // solution: (u1^(1/3) + u2^(1/3) - a) / 3

    auto const det = p * p - q * q * q;
    auto const u0 = p + dim::sqrt(det);
    auto const u1 = p - dim::sqrt(det);
    auto const umax = dim::norm(u0) >= dim::norm(u1) ? u0 : u1;

    auto const z0 = dim::pow<1, 3>(umax);
    auto const z1 = z0.value == S(0) ? z0 : q / z0; // (z0 * z1 = q) must hold

    T const halfsqrt3 = T(0.866025403784438646);
    auto const w = S({-0.5,  halfsqrt3});
    auto const v = S({-0.5, -halfsqrt3});

    return gvector<C, Q>
    {
        (z0     + z1     - poly.a),
        (z0 * w + z1 * v - poly.a),
        (z0 * v + z1 * w - poly.a),
    } / S(3);
}
template<typename T, quantity Q>
auto solveCubicEquation(CubicPolynomial<T, Q> const &poly) noexcept
{
    CubicPolynomial<std::complex<T>, Q> const p = {poly.a, poly.b, poly.c};
    return solveCubicEquation(p);
}

enum class SolutionKind
{
    Distinct,
    TwiceDegenerate,
    ThriceDegenerate,
};
template<typename T, quantity Q>
struct ClassifiedSolution
{
    using C = std::complex<T>;
    gvector<C, Q> value;
    SolutionKind kind;

    ClassifiedSolution(gvector<C, Q> const &e) noexcept
    {
        using S = gscalar<C, Q>;
        auto const equal = [eps = 128 * std::numeric_limits<T>::epsilon()](S const a, S const b) noexcept
        {
            return eps * max(abs(a), abs(b)) >= abs(a - b);
        };
        int const eq[3] =
        {
            equal(e[1], e[2]) ? 1 : 0,
            equal(e[2], e[0]) ? 1 : 0,
            equal(e[0], e[1]) ? 1 : 0,
        };
        int const sum = eq[0] + eq[1] + eq[2];
        if(sum == 0)
        {
            value = e;
            kind = SolutionKind::Distinct;
        }
        else if(sum == 1)
        {
            unsigned int const i = eq[0] ? 0 : (eq[1] ? 1 : 2);
            value = {e[i], e[(i + 1) % 3], e[(i + 2) % 3]};
            kind = SolutionKind::TwiceDegenerate;
        }
        else
        {
            value = e;
            kind = SolutionKind::ThriceDegenerate;
        }
    }
};

} // namespace dim
