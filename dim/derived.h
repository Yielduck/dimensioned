#pragma once
#include "quantity.h"
namespace dim
{

static constexpr quantity Frequency                 = inv(Time);

static constexpr quantity Area                      = pow<2>(Length);
static constexpr quantity Volume                    = pow<3>(Length);

static constexpr quantity Velocity                  = div(Length, Time);
static constexpr quantity Acceleration              = div(Velocity, Time);

static constexpr quantity GravitationalParameter    = mul(Area, Acceleration);

static constexpr quantity Momentum                  = mul(Mass, Velocity);
static constexpr quantity Force                     = mul(Mass, Acceleration);

static constexpr quantity AngularVelocity           = div(None, Time);
static constexpr quantity AngularAcceleration       = div(AngularVelocity, Time);

static constexpr quantity AngularMomentum           = mul(Length, Momentum);
static constexpr quantity Torque                    = mul(Length,    Force);

static constexpr quantity InertiaMoment             = div(AngularMomentum, AngularVelocity);

static constexpr quantity Pressure                  = div(Force, Area);
static constexpr quantity Energy                    = mul(Length, Force);
static constexpr quantity Power                     = div(Energy, Time);
static constexpr quantity Action                    = mul(Energy, Time);

static constexpr quantity HeatCapacity              = div(Energy, Temperature);
static constexpr quantity Entropy                   = HeatCapacity;

static constexpr quantity Concentration             = div(None, Volume);
static constexpr quantity Density                   = div(Mass, Volume);

static constexpr quantity MassFlux                  = mul(Density, Velocity);
static constexpr quantity EnergyFlux                = mul(Pressure, Velocity);

static constexpr quantity Viscosity                 = mul(Pressure, Time);
static constexpr quantity KinematicViscosity        = div(Viscosity, Density);

static constexpr quantity MolarMass                 = div(Mass, AmountOfSubstance);

static constexpr quantity ElectricCharge            = mul(ElectricCurrent, Time);
static constexpr quantity ChargeDensity             = div(ElectricCharge, Volume);
static constexpr quantity CurrentDensity            = mul(ChargeDensity, Velocity);

static constexpr quantity Voltage                   = div(Energy, ElectricCharge);
static constexpr quantity Resistance                = div(Voltage, ElectricCurrent);
static constexpr quantity ElectricCapacity          = div(ElectricCharge, Voltage);

static constexpr quantity ElectricField             = div(Force, ElectricCharge);
static constexpr quantity ElectricInduction         = div(ElectricCharge, Area);

static constexpr quantity MagneticField             = mul(CurrentDensity, Length);
static constexpr quantity MagneticMoment            = mul(ElectricCurrent, Area);
static constexpr quantity MagneticInduction         = div(Torque, MagneticMoment);

static constexpr quantity Bandwidth                 = div(Information, Time);

} // namespace dim
