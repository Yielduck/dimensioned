This library provides the utilities for handling physical dimensioned quantities.

The purpose is to bring the elements of dimensional analysis into the C++ type system which helps to discover various mistakes at compile-time and provides clarity and explicity for function signatures and data sturctures.

Inside namespace `dim`, it defines
- `using quantity = std::int64_t;`
- seven base quantities: `Time`, `Length`, `Mass`, `ElectricCurrent`, `Temperature`, `AmountOfSubstance`, `LuminousIntensity`
- zero quantity, `None`
- basic operations on quantities: `mul`, `div`, `pow`, `inv`, `sqrt`
- seven SI defining constants and a couple of the other useful ones
- scaling prefixes like `kilo` and `micro`
- basic units for base and derived quantities like `meter`, `arcsecond`, or `calorie`
- derived quantities like `static constexpr quantity Energy = mul(Length, Force);`
- `template<quantity>` structures `scalar`, `vector`, `matrix` with overloaded operators and useful functions
- solver for cubic equation and matrix exponential
