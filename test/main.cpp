#include <dim/unit.h>
#include <dim/matrix.h>
#include <dim/write.h>
#include <gtest/gtest.h>

TEST(dimension, mul)
{
    EXPECT_EQ(dim::mul(dim::None, dim::None  ), dim::None);
    EXPECT_EQ(dim::mul(dim::Time, dim::Length), 0x777777777777788);
    EXPECT_EQ(dim::mul(dim::Time, dim::Time  ), 0x777777777777779);
}
TEST(dimension, div)
{
    EXPECT_EQ(dim::div(dim::None, dim::None  ), dim::None);
    EXPECT_EQ(dim::div(dim::Time, dim::Time  ), dim::None);
    EXPECT_EQ(dim::div(dim::Time, dim::Length), 0x777777777777768);
}
TEST(dimension, pow)
{
    EXPECT_EQ(dim::pow<0>(dim::Time), dim::None);
    EXPECT_EQ(dim::pow<1>(dim::Time), dim::Time);

    EXPECT_EQ((dim::pow< 8   >(dim::Time)), 0x77777777777777f);
    EXPECT_EQ((dim::pow<-7   >(dim::Time)), 0x777777777777770);

    dim::quantity const time6 = dim::pow<6>(dim::Time);
    EXPECT_EQ((dim::pow<1, 6>(time6)), dim::Time);
    EXPECT_EQ((dim::pow<1, 3>(time6)), dim::mul(dim::Time, dim::Time));
}

TEST(vec3, vec3)
{
    dim::vector<dim::None> const e1 = {1., 0., 0.};
    dim::vector<dim::None> const e2 = {0., 1., 0.};
    dim::vector<dim::None> const r = {1., 2., 4.};
    EXPECT_EQ(dim::cross(e1, e2).z, 1.);
    EXPECT_EQ(dim::length(dim::cross(e1, e2)), 1.);
    EXPECT_EQ(dim::length(dim::cross(r, r)), 0.);
    EXPECT_EQ(dim::length(r), std::sqrt(21.));
}
TEST(vec3, ops)
{
    dim::vector<dim::Length> const l = {dim::unit::meter, 0, dim::unit::meter};
    dim::vector<dim::Area> const a = {dim::unit::squareMeter, dim::unit::squareMeter, 0};
    dim::vector<dim::Volume> const v0 = l * a;
    dim::vector<dim::Volume> const vl = v0 * 2.f;
    dim::vector<dim::Volume> const vr = 3 * v0;
    EXPECT_EQ(dim::length(v0),     dim::unit::cubicMeter);
    EXPECT_EQ(dim::length(vl), 2 * dim::unit::cubicMeter);
    EXPECT_EQ(dim::length(vr), 3 * dim::unit::cubicMeter);
}
TEST(mat3, ops)
{
    dim::matrix<dim::Length> const m =
    {
        {1, 1, 1},
        {0, 2, 1},
        {0, 0, 3},
    };
    dim::matrix<dim::Area> const a = m * m;
    EXPECT_EQ(dim::trace(a), 14.);
}

TEST(mat3, characteristicPoly)
{
    dim::matrix<dim::Length> const m =
    {
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9},
    };
    auto const poly = dim::characteristicPolynomial(m);

    EXPECT_EQ(poly.a, -15.);
    EXPECT_EQ(poly.b, -18.);
    EXPECT_EQ(poly.c,   0.);

    for(auto const x : dim::solveCubicEquation(poly))
        EXPECT_EQ(poly(dim::real(x)), 0.);
}
TEST(mat3, exp)
{
    using C = std::complex<float>;
    dim::gmatrix<C, dim::Length> const m =
    {
        {C(0, 1), C(0), C(0)},
        {C(0), C(0, 1), C(0)},
        {C(0), C(0), C(0, 0)},
    };
    auto const p = dim::phi3<float, dim::Length>(m);
    auto const a = p(dim::gscalar<C, dim::inv(dim::Length)>(1e4));
    EXPECT_EQ(a[0][0].value, std::exp(C(0, 1e4)));
    EXPECT_EQ(a[2][2].value, std::exp(C(0,   0)));
}
TEST(mat3, inverse)
{
    dim::gmatrix<float, dim::Time> const m =
    {
        {1, 1, 0},
        {0, 1, 1},
        {0, 0, 1},
    };
    dim::gmatrix<float, dim::inv(dim::Time)> const minv =
    {
        {1, -1,  1},
        {0,  1, -1},
        {0,  0,  1},
    };
    for(unsigned int i = 0u; i < 3u; ++i)
        for(unsigned int j = 0u; j < 3u; ++j)
            EXPECT_EQ(dim::inverse(m)[i][j], minv[i][j]);
}
TEST(mat3, complex)
{
    auto const m = dim::identity<std::complex<double>>;
    EXPECT_EQ(dim::determinant(m), dim::determinant(dim::inverse(m)));
}
TEST(mat3, mul)
{
    dim::matrix<dim::Length> const m =
    {
        {1., 2., 3.},
        {4., 5., 6.},
        {7., 8., 9.},
    };
    dim::matrix<dim::Area> const m2 =
    {
        {30., 36., 42.},
        {66., 81., 96.},
        {102., 126., 150.},
    };
    for(unsigned int i = 0u; i < 3u; ++i)
        for(unsigned int j = 0u; j < 3u; ++j)
            EXPECT_EQ((m * m)[i][j], m2[i][j]);
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
